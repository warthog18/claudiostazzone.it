---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Poesie"
subtitle: ""
summary: "Alcune poesie scritte molti anni fa..."
authors: [admin]
tags: []
categories: []
date: 2021-09-15T21:13:36+02:00
lastmod: 2021-09-15T21:13:36+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

In questa sezione ho raccolto alcune poesie, scritte in psrticolari momenti della mia vita.

La scelta di pubblicarle su quwsto sito è stata dura, perché sono delle poesie abbastanza personali, 
ma che potrebbero servire ad altre persone che hanno provato le mie stesse sensazioni.
Inoltre è sempre utile ricevere un giudizio del lettore.

Queste poesie sono state un aiuto in alcuni momenti difficili ed è un esercizio
molto valido per affinare di più la mia conoscenza della lingua inglese.
